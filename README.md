# README #

## Running application ##

* Install dependencies: go to helper/setup.bat (for use setup.sh for Linux OS)
* Start API: go to helper/start-api.bat to run API
* Start Web app: go to helper/start-web.bat to run Web app

After all run all commands, you can access to the application at http://localhost:3000


## Contribution guidelines ##
### Contributing to the current release ###
Using SourceTree is recommended but not compulsory, these notes assumes SourceTree is being used.

See - [Git Flow in SourceTree](http://blog.sourcetreeapp.com/2012/08/01/smart-branching-with-sourcetree-and-git-flow)

The basic process we will use is as described in the link above but with some manual alterations.
Unfortunately, using the Source Tree Git flow features tends to lead to merge of changes directly to the **develop** branch, which is not suitable for this repository, we need to create a **pull request** instead.

* Check out the develop branch.
* From the Repository Menu in SourceTree, select Git Flow -> Initialize Repository (you only need to do it at the first time you clone the repository to initialize the flow) 
* From the Repository Menu in SourceTree, select Git Flow -> **Start New Feature**.
* Enter the name of your feature using the Jira Story and Description (e.g. VZW-1 - Define and setup project skeleton). After that SourceTree will create a branch with name **feature/VZW-1_-_Define_and_setup_project_skeleton**
* Select the branch from which to start (usually develop).
* SourceTree will then create a branch called /feature/<branch name>
* You can then make and commit changes until you are ready to have your changes reviewed.
* Once you are happy with changes, push your changes to Bitbucket. Make sure you are pushing to a remote branch with the same name as your branch (not develop).
* You can push your changes as often as you like to your remote branch.
* When you changes are finalized, **create a pull request** so your changes can be reviewed. This can be done by right-clicking your branch or from within Bitbucket directly.
* Usually, you can select the option to delete the branch after the merge.
* Select at least one reviewer
* Usually you will be pushing to the develop branch.
* Hoang Le (or another authorized individual) will then review the code change and either accept or reject the merge.
* Any of the reviewers can **Approve** the pull request but this, in itself, does not merge the code.

### Contributing to a previous release ###
The process for this is similar to the above but the start and destination branches will be one of the version branches. E.G. version/1.0.0.
This means you'll branch from the version branch and then create a pull request back to the version branch.

### Release Procedure - Current Release ###

When we are ready to start preparing for the release of the next version, we will create a release branch. This will typically be done in the last few days of the last sprint of the release.
At this stage, **all the features should be completed** and the only things left to do are documentation, configuration, and, perhaps some bug fixing.
The release branch will be created by the Lead Developer (Hoang).

* Create a branch called release/1.0.0 (use the appropriate release number).
* Make any changes required for the release.
* Merge back any changes to the develop branch and the Master branch.
* Tag the master branch with the release number e.g. 1.0.0.
* Delete the release branch when done.
* Create a new version branch from the tag in master, e.g. version/1.0.0. this will be used for Hot Fixing (see below).
* Remove any version branches that are no longer under code support.