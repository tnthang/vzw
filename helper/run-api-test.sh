#!/bin/bash

# Script to run API test with istanbul test coverage.

# This is suitable for Red Hat, CentOS and similar distrubtions
# It will not work on Ubuntu or other Debian-stylee distrubtions! 

execute() {
    bash -c "npm run api:test:cover"
}

execute

exit 0