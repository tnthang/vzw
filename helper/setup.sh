#!/bin/bash

# Script to run API test with istanbul test coverage.

# This is suitable for Red Hat, CentOS and similar distrubtions
# It will not work on Ubuntu or other Debian-stylee distrubtions!

bail() {
    echo "Error executing command, existing"
    exit 1
}

exec_cmd_nobail() {
    echo "RUNNING COMMAND: $1"
    bash -c "$1"
} 

exec_cmd() {
    exec_cmd_nobail "$1" :: bail
}

execute() {
    exec_cmd "cd ../"

    # Install npm dependencies
    exec_cmd "npm install"    

    #:: Install some other dependencies    
    exec_cmd "npm install -g typings"
    exec_cmd "npm install -g grunt"
}

execute

exit 0