:: Before running setup file, please make sure you have the Node.js version >=6.9.1 installed in your machine

@echo off

echo Please wait for a few minutes to install all needed dependencies...

cd ../

:: Install NPM packages
echo Installing npm packages..
npm install

:: Install some other dependencies
echo Installing global npm dependencies...
npm install -g typings
npm install -g grunt

pause