#!/bin/bash

# Script to run API test with istanbul test coverage.

# This is suitable for Red Hat, CentOS and similar distrubtions
# It will not work on Ubuntu or other Debian-stylee distrubtions!

execute() {
    exec_cmd "cd ../"
    
    exec_cmd "npm start"    
 
    exec_cmd "pause"
}

execute

exit 0