var request = require('supertest-as-promised');
var chai = require('chai');
var expect = chai.expect;

describe('UserController', () => {
    describe('GET /users', () => {
        it('should return success', () => {
            return request(sails.hooks.http.app)
                .get('/users')
                .expect(200);
        });

        it('should return 2 users', () => {
            return request(sails.hooks.http.app)
                .get('/users')
                .expect(200)
                .then((res) => {
                    return expect(res.body.length).to.equal(2);
                });
        })
    });

    xdescribe('POST /users', () => {
        xit('should able to post user', () => {

        });
    });
});