const sails = require('sails');
const sequelize_fixtures = require('sequelize-fixtures');

before(function (done) {
    // Increase the Mocha timeout so that Sails has enough time to lift.
    this.timeout(20000);

    // Starting sails server
    sails.lift({
        hooks: {
            orm: false,
            pubsub: false,
            grunt: false,
            "_beforeAllHooks": require('../api/hooks/_beforeAllHooks'),
            sequelize: require('../api/hooks/sequelize')
        },
        log: {
            level: 'error'
        },
        models: {
            connection: 'mysql',
            migrate: 'drop'
        }
    }, (err, server) => {
        if (err) return done(err);

        // Loading fixtures to populate data for testing
        const models = sails.models;
        sequelize_fixtures
            .loadFile(__dirname + '/fixtures/*.json', models)
            .then(() => {
                done(null, sails);
            });
    });
});

after(function (done) {
    console.log(); // Skip a line before displaying Sails lowering logs
    sails.lower();
    done();
});