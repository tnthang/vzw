/**
 * 200 (OK) Response
 *
 * Usage:
 * return res.generic();
 * return res.generic(data); 
 *
 * @param  {Object} data            The response data will be returned in the body
 * @param  {String} statusCode      The status code to be returned 
 */
module.exports = function genericResponse(data, statusCode) {
    // Get access to `req`, `res`, & `sails`
    var req = this.req;
    var res = this.res;
    var sails = req._sails;

    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Max-Age', 0);
    res.header('Cache-Control', 'no-cache');
    res.header('Expires', '-1');

    // Default status code to 200 if don't specify any value
    if (!statusCode) {

        statusCode = 200;
    }

    // Set status code
    res.status(statusCode);

    return res.jsonx(data);
};
