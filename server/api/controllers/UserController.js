/**
 * UserController
 *
 * @description :: Server-side logic for UserController
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var BaseController = require('./BaseController');

// Create UserController based on BaseController
var UserController = function () { };
UserController.prototype = Object.create(BaseController.prototype);

/**
**************************************************************************************************
************************************ Customer methods                    *************************
**************************************************************************************************
*/
/**
 * Get all users
 * @param  {Object} req     The request object
 * @param  {Object} res     The response object
 */
UserController.prototype.getAll = function (req, res) {
    User.findAll({})
        .then(function (users) {
            res.genericResponse(users, 200);
        })
        .catch((err) => {
            res.json(err);
        });
}

module.exports = UserController.prototype;