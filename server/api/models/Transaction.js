/**
 * Transaction.js
 *
 * @description :: ORM model definition for Transaction model
 * @help        :: See http://docs.sequelizejs.com/en/latest/docs/models-definition/
 */

module.exports = _.merge(_.cloneDeep(require('./Base')), {
    attributes: {
        total: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true
        }
    },
    options: {
        tableName: 'transaction',
        classMethods: {},
        instanceMethods: {},
        hooks: {}
    }
});