/**
 * Base.js
 *
 * @description :: ORM model definition for Base, this is a base model that can be extended
 * @help        :: See http://docs.sequelizejs.com/en/latest/docs/models-definition/
 */

module.exports = {
    attributes: {
        isDeleted: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }
};