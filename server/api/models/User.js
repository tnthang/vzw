/**
 * User.js
 * 
 * @description :: ORM model definition for User model
 * @help        :: See http://docs.sequelizejs.com/en/latest/docs/models-definition/
 */

module.exports = _.merge(_.cloneDeep(require('./Base')), {
    attributes: {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        age: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        tableName: 'user',
        classMethods: {},
        instanceMethods: {},
        hooks: {}
    }
});