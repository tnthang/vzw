module.exports = function (grunt) {

  grunt.initConfig({
    mocha_istanbul: {
      coverage: {
        src: 'test',
        options: {
          coverageFolder: 'coverage',
          mask: '**/*.test.js',
          mochaOptions: ['--harmony', '--async-only'], // any extra options
          istanbulOptions: ['--harmony', '--handle-sigint'],
          root: 'api',
          check: {
            lines: 20,
            statements: 20,
            functions: 20,
            statements: 20
          },
          exclues: []
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-mocha-istanbul');

  // Adding test task enabling "grunt test" command
  grunt.registerTask('test', [
    'mocha_istanbul:coverage'
  ]);
};