describe('App', () => {

    beforeEach(() => {
        browser.get('/');
    });

    it('should have a title', () => {
        let subject = browser.getTitle();
        let result = 'VZW Prepaid Activation';
        expect(subject).toEqual(result);
    });
});